# Terraform Style Guide

Standards for writing high quality Terraform code

## Naming Conventions

### File Names

Group resources and data sources by their purpose within the module and name the Terraform file according to its purpose.

> For example, EC2 instances are being created for an EKS cluster. They should live in `workers.tf` rather than `ec2.tf` or `autoscaling_group.tf`.

Colocate strongly related resources in the same file.

> Suppose we are creating an S3 bucket and an IAM policy to support backups. We should put them in `backups.tf` rather than `s3.tf` and `iam.tf`.

### Resource and Data Source Names

Give resources names that describe how they are used within the context of the module. Avoid repeating the type or provider of the resource in its name. When an engineer does `terraform state list`, she should see a collection of resources that are related in a logical way and describe the constituent parts.

Example of good description
```
resource "aws_s3_bucket" "backup" {
  ...
}
```

Example of redundant naming
```
resource "aws_s3_bucket" "backup_bucket" {
  ...
}
```

### Terraform resource names vs attribute names

Words within resource names should be separated by an underscore (`_`). When a resource has a `name` or some other user-selectable identifier, words should be separated by dashes (`-`) if required by the provider.

Consider the following:
```
resource "google_compute_instance" "boundary_workers" {
  count = var.workers
  name  = "boundary-worker-${count.index}"
  ...
}
```


## Readability and Editor Ergonomics

### Spacing

Indent the contents of blocks, multiline lists, objects, and maps with two spaces.

### Lists

When a list has one element, put it on the same line with no comma:

```
some_list = ["a value"]
```

When a list has more than one element, put the elements on separate lines.

```
some_list = [
  "a value",
  "another value",
]
```

Notice the comma after the final element. HCL allows a comma after the final element, which is convenient because it allows elements to be appended while maintaining the clarity of Git diffs.

### Avoid heredocs

Heredocs make syntax highlighting less effective, which can make it easier to visually miss syntax errors. Consider rewriting the code so that heredocs are not necessary.

For example, the [aws_iam_policy_document](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) allows us to write HCL instead of hand-writing AWS JSON policy documents where it might be tempting to use a heredoc.
